FROM ubuntu:18.04

WORKDIR /opt/toolchain
SHELL ["/bin/bash", "-c"]

ADD toolchain-src-249.0.tar.bz2 /opt

RUN apt-get update && apt-get install -y nano curl gawk build-essential python python2.7-dev bison flex libmpfr-dev babeltrace pkg-config libiberty-dev texinfo
RUN curl -L https://install.perlbrew.pl | bash && /root/perl5/perlbrew/bin/perlbrew install -j12 5.22.1
RUN printf '%s\n%s\n' 'source /root/perl5/perlbrew/etc/bashrc' '/root/perl5/perlbrew/bin/perlbrew switch perl-5.22.1' >> /root/.bash_profile

ENV PATH=/root/perl5/perlbrew/bin:/root/perl5/perlbrew/perls/perl-5.22.1/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# UNCOMMENT TO BUILD FULL TOOLCHAIN
# RUN make linux

# TO STATICALLY BUILD GDBSERVER:
# cd /opt/toolchain/gits/gdb/gdb/gdbserver
# ./configure --host=mips64-octeon-linux-gnu --target=mips64-octeon-linux-gnu --prefix="/opt/toolchain/tools/" CXXFLAGS="-fPIC -static"
# make -j gdbserver GDBSERVER_LIBS="/opt/toolchain/tools/mips64-octeon-linux-gnu/lib64/octeon2/libstdc++.a /opt/toolchain/tools/lib/gcc/mips64-octeon-linux-gnu/7.3.0/octeon2/libgcc_eh.a /opt/toolchain/tools/mips64-octeon-linux-gnu/sys-root/usr/lib64/octeon2/libdl.a"

ENTRYPOINT bash
